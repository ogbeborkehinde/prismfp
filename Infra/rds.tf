
resource "random_password" "sre-task-db-password" {
  length           = 8
  upper            = false
  numeric          = true
  special          = false
  override_special = "_%@"
}
resource "aws_secretsmanager_secret" "sre-task-db" {
  name = var.prefix
}
resource "aws_secretsmanager_secret_version" "sre-task-db" {
  secret_id     = aws_secretsmanager_secret.sre-task-db.id
  secret_string = random_password.sre-task-db-password.result
}

resource "aws_db_subnet_group" "sre-task" {
  name       = "${var.prefix}-subnet-group"
  subnet_ids = module.vpc.public_subnets
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 5.0"

  name        = var.prefix
  description = "Complete PostgreSQL security group"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 5432
      to_port     = 5432
      protocol    = "tcp"
      description = "PostgreSQL access from within VPC"
      cidr_blocks = module.vpc.vpc_cidr_block
    },
  ]
}

module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier                     = "demodb"
  instance_use_identifier_prefix = true

  create_db_option_group    = false
  create_db_parameter_group = false

  engine               = "postgres"
  engine_version       = "14"
  family               = "postgres14"
  major_engine_version = "14"
  instance_class       = "db.t3.micro"

  allocated_storage = 5
  
  manage_master_user_password = false
  
  db_name  = "demo"
  username = "demo"
  password = random_password.sre-task-db-password.result
  port     = 5432

  db_subnet_group_name   = aws_db_subnet_group.sre-task.name
  vpc_security_group_ids = [module.security_group.security_group_id]

  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = 0
}

# resource "kubernetes_secret" "database" {
#   metadata {
#     name = var.prefix
#     #namespace = "sre-..."  # Insert your namespace here
#     namespace = kubernetes_namespace.sre-task.id
#   }

#   data = {
#     DATABASE_URI            = "postgresql://demo:${random_password.sre-task-db-password.result}@${module.db.db_instance_endpoint}/demo"
#     DATABASE_SECRET_KEY     = "zxaWOJ-x!a1pY-RpV$q6-xTqVoE-!9g4kX-WOuFBd"
#     DATABASE_JWT_SECRET_KEY = "pXVHy!-QhSUaP-!9ySpa-dBSMKN-1UtSh8-ueBLXf"
#   }

#   depends_on = [kubernetes_namespace.sre-task]
# }

# resource "kubernetes_namespace" "sre-task" {
#   metadata {
#     name = "sre-task"
#   }
# }