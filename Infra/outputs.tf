output "cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = module.eks.cluster_name
}

output "cluster_region" {
  description = "Kubernetes Cluster Region"
  value       = var.aws_region
}

output "cluster_endpoint" {
  description = "Endpoint for EKS control plane"
  value       = module.eks.cluster_endpoint
}

output "db_instance_endpoint" {
  description = "The connection endpoint"
  value       = module.db.db_instance_endpoint
}

output "db_instance_status" {
  description = "The RDS instance status"
  value       = module.db.db_instance_status
}

output "URI" {
  description = "The URI of the RDS database"
  value       = "postgresql://demo:${nonsensitive(random_password.sre-task-db-password.result)}@${module.db.db_instance_endpoint}/demo"
}