# -*- encoding: utf-8 -*-
from datetime import timedelta
import os


class BaseConfig():
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "zxaWOJ-x!a1pY-RpV$q6-xTqVoE-!9g4kX-WOuFBd"
    JWT_SECRET_KEY = "pXVHy!-QhSUaP-!9ySpa-dBSMKN-1UtSh8-ueBLXf"
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)
