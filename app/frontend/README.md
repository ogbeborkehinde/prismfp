# Frontend

UI that allows clients to register in our system.

## Quick start

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
